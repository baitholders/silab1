import msrcpsp.io.MSRCPSPIO;
import msrcpsp.scheduling.*;
import msrcpsp.scheduling.greedy.Greedy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Piotr on 11.03.2017.
 */
public class Solver {
    private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());
    private static final String definitionFile = "assets/def_small/10_3_5_3.def";
    private static final String writeFile = "assets/solutions_small/15_9_12_9.sol";

    public static void main(String[] args) {

        MSRCPSPIO reader = new MSRCPSPIO();
        Schedule schedule = reader.readDefinition(definitionFile);

        if (null == schedule) {
            LOGGER.log(Level.WARNING, "Could not read the Definition " + definitionFile);
        }

        Greedy greedy = new Greedy(schedule.getSuccesors());
        RandomSchedule generator = new RandomSchedule(new Random(System.currentTimeMillis()));
        SchedulePopulationGenerator gen = new SchedulePopulationGenerator(schedule, generator, greedy);
        List<Schedule> schedules = gen.generate(100);

        Population population = new Population(schedules);
        GeneticAlgorithm genetic = new GeneticAlgorithm(population);
        System.out.println(population.getMinAvgMax());
        System.out.println();


        //Generations, Crossover, Muation, Tournament
        genetic.start(200, 0.3, 0.02, 30);


        List<List<Integer>> list = genetic.getMinAvgMax();
        System.out.println("Size = " + list.size());
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new File("test.csv"));
        } catch (FileNotFoundException e) {

        }
        StringBuilder sb = new StringBuilder();

        for(List<Integer> list1 : list) {
            sb.append(list1.get(0));
            sb.append(',');
            sb.append(list1.get(1));
            sb.append(',');
            sb.append(list1.get(2));
            sb.append('\n');
        }

        pw.write(sb.toString());
        pw.close();
        System.out.println("done!");
    }

    public static void printSchedule(Schedule schedule) {
        System.out.println("======================================================================================================");
        System.out.println("Tasks");
        for (Task t : schedule.getTasks())
            System.out.println(t.toString());

        System.out.println("Resources");
        for (Resource r : schedule.getResources())
            System.out.println(r.toString());
        System.out.println("======================================================================================================");
    }
}
