package msrcpsp.scheduling;

import msrcpsp.scheduling.greedy.Greedy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piotr on 11.03.2017.
 */
public class SchedulePopulationGenerator {
    private Schedule schedule;
    private RandomSchedule generator;
    private Greedy greedy;

    public SchedulePopulationGenerator(Schedule schedule, RandomSchedule generator, Greedy greedy) {
        this.schedule = schedule;
        this.generator = generator;
        this.greedy = greedy;
    }

    public List<Schedule> generate(int size) {
        List<Schedule> schedules = new ArrayList<Schedule>();
        for(int i = 0; i < size; i++)
            schedules.add(getPlannedSchedule());
        return schedules;
    }

    private Schedule getPlannedSchedule() {
        Schedule schedule = randomlyAssignedSchedule();
        greedy.setHasSuccessors(schedule.getSuccesors());
        return greedy.buildTimestamps(schedule);
    }

    private Schedule randomlyAssignedSchedule() {
        return generator.randomTasksAssignment(schedule);
    }
}
