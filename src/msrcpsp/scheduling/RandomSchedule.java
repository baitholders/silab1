package msrcpsp.scheduling;

import java.util.List;
import java.util.Random;

/**
 * Created by Piotr on 11.03.2017.
 */

public class RandomSchedule {
    private Random random;

    public RandomSchedule(Random random) {
        this.random = random;
    }

    public Schedule randomTasksAssignment(Schedule schedule) {
        Task[] tasks = schedule.getTasks();
        int[] upperBounds = schedule.getUpperBounds(schedule.getTasks().length);
        randomlyAssignedSchedule(schedule, tasks, upperBounds);
        Schedule s = new Schedule(schedule);
        s.clear(false);
        return s;
    }

    private void randomlyAssignedSchedule(Schedule schedule, Task[] tasks, int[] upperBounds) {
        List<Resource> capableResources;
        for (int i = 0; i < tasks.length; ++i) {
            capableResources = schedule.getCapableResources(tasks[i]);
            schedule.assign(tasks[i], getRandomCapableResource(capableResources, upperBounds[i]));
        }
    }

    private Resource getRandomCapableResource(List<Resource> resources, int numberOfResources) {
        return resources.get(getRandomIndex(numberOfResources));
    }

    private int getRandomIndex(int numberOfResources) {
        return (int) (random.nextDouble() * numberOfResources);
    }
}
