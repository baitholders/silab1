package msrcpsp.scheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Piotr on 11.03.2017.
 */
public class Population {
    private List<Schedule> schedules;
    private List<Double> evaluations;
    private int size;

    public Population(List<Schedule> schedules) {
        this.schedules = schedules;
        this.size = schedules.size();
        this.evaluations = schedules.stream().map(Schedule::evaluate).collect(Collectors.toList());
    }

    public int getSize() {
        return size;
    }

    public Schedule getScheduleByIndex(int index) {
        return schedules.get(index);
    }

    public List<Integer> getMinAvgMax() {
        List<Integer> minAvgMax = new ArrayList<Integer>();
        minAvgMax.add(Collections.min(evaluations).intValue());
        minAvgMax.add((int)evaluations.stream().mapToDouble(a -> a).average().getAsDouble());
        minAvgMax.add(Collections.max(evaluations).intValue());
        return minAvgMax;
    }

    public List<Double> getEvaluations() {
        return evaluations;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }
}
