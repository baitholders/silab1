package msrcpsp.scheduling;

import msrcpsp.io.MSRCPSPIO;

import java.util.HashMap;
import java.util.List;

public class Pop {

    Schedule baseSchedule;
    Schedule[] population;
    int size;

    private static final String definitionFile = "assets/def_small/10_3_5_3.def";

    public Pop(int size) {

        MSRCPSPIO generator=new MSRCPSPIO();
        baseSchedule=generator.readDefinition(definitionFile);
        this.size=size;
        population=new Schedule[size];
        generatePopulation();
    }

    private void generatePopulation() {
        HashMap<Task, List<Resource>> mapping=getBaseTaskResourceMapping();
        for(int i=0; i<size ; i++){
            population[i]=new Schedule(baseSchedule);
            System.out.println("len = " + population[i].getTasks().length);
            for(Task t: population[i].getTasks()){
                System.out.println("null? " + t==null);
                List<Resource> resources = mapping.get(t);
                System.out.println("null? " + resources==null);
                Resource randomResource = getRandomResource(resources);
                System.out.println("Random = " + randomResource);
                population[i].assign(t, randomResource);
                System.out.println("index aft " + population[i].getTasks()[0].getResourceId());
            }
        }
        System.out.println();
    }

    private HashMap<Task, List<Resource>> getBaseTaskResourceMapping(){
        HashMap<Task, List<Resource>> result=new HashMap<Task,List<Resource>>();
        for(Task t: baseSchedule.getTasks()){
            result.put(t, baseSchedule.getCapableResources(t));
        }
        return result;
    }

    private Resource getRandomResource(List<Resource> resources){
        System.out.println("null");
        System.out.println(resources == null);
        int index = (int) (Math.random() * resources.size());
        System.out.println("index = " + index);
        System.out.println();
        return resources.get(index);
    }

    public static void main(String [] args){

        Pop pop=new Pop(10);
        for(Schedule s: pop.population){
            System.out.println(s.resourcesAssigments());
        }
    }

}