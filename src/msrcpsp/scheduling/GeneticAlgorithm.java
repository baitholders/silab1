package msrcpsp.scheduling;

import msrcpsp.scheduling.greedy.Greedy;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Piotr on 11.03.2017.
 */

public class GeneticAlgorithm {
    private Population population;
    private Greedy greedy;
    private Random random;
    private List<List<Integer>> minAvgMax;

    public GeneticAlgorithm(Population population) {
        this.population = population;
        this.random = new Random(System.currentTimeMillis());
        this.greedy = new Greedy();
        minAvgMax = new ArrayList<>();
    }

    public Population start(int generations, double crossover, double mutation, int tournament) {
        for(int i = 0; i < generations; i++) {
            System.out.println("Generation: " + i);
            List<Schedule> schedules = new ArrayList<Schedule>();

            while(schedules.size() < population.getSize()) {
                int selected = selectSchedule(tournament);
                Schedule newSchedule1 = new Schedule(population.getScheduleByIndex(selected));
                newSchedule1.clear(false);
                if (getProbability() < crossover) {
                    selected = selectSchedule(tournament);
                    Schedule newSchedule2 = new Schedule(population.getScheduleByIndex(selected));
                    newSchedule2.clear(false);
                    schedules.addAll(crossoverSchedules(newSchedule1, newSchedule2, mutation));
                }
                else {
                    mutation(newSchedule1, mutation);
                    schedules.add(newSchedule1);
                    System.out.println("------------------");
                    System.out.println(newSchedule1.resourcesAssigments());
                    System.out.println("------------------");
                }
            }
            for(Schedule s : schedules) {
                greedy.setHasSuccessors(s.getSuccesors());
                greedy.buildTimestamps(s);
            }
            population = new Population(schedules);
            minAvgMax.add(population.getMinAvgMax());
        }

        return null;
    }

    private List<Schedule> crossoverSchedules(Schedule newSchedule1, Schedule newSchedule2, double mutation) {
        int tasksNum = newSchedule1.getTasks().length;
        int split = getInfFromRange(0, tasksNum);
        List<Schedule> schedules = new ArrayList<Schedule>();

        for(int i = split; i < tasksNum; i++)
            crossResources(newSchedule1.getTasks()[i], newSchedule2.getTasks()[i]);

        mutation(newSchedule1, mutation);
        mutation(newSchedule2, mutation);
        schedules.add(newSchedule1);
        schedules.add(newSchedule2);
        return schedules;
    }

    private void mutation(Schedule newSchedule, double mutation) {
        double mutationProbability;
        Task[] tasks = newSchedule.getTasks();
        List<Resource> capableResources;
        int[] upperBounds = newSchedule.getUpperBounds(newSchedule.getTasks().length);

        for(int i = 0; i < newSchedule.getTasks().length; i++) {
            mutationProbability = random.nextDouble();
            if(mutationProbability < mutation) {
                capableResources = newSchedule.getCapableResources(tasks[i]);
                int actualResourceId = tasks[i].getResourceId();
                int tries = 5;

                Resource randomCapableResource = getRandomCapableResource(capableResources, upperBounds[i]);
                while(randomCapableResource.getId() == actualResourceId && tries > 0) {
                    randomCapableResource = getRandomCapableResource(capableResources, upperBounds[i]);
                    tries--;
                }
                newSchedule.assign(tasks[i], randomCapableResource);
            }
        }
    }

    private void crossResources(Task task, Task task1) {
        int resource1 = task.getResourceId();
        task.setResourceId(task1.getResourceId());
        task1.setResourceId(resource1);
    }

    private int selectSchedule(int tournament) {
        Double minEvaluation = Double.MAX_VALUE;
        Set<Integer> indexSet = new HashSet<Integer>();
        return getIndexMinScheduleEvaluation(tournament, minEvaluation, indexSet);
    }

    private int getIndexMinScheduleEvaluation(int tournament, Double evaluationMin, Set<Integer> indexSet) {
        int indexMin = -1;
        double evaluate;
        while(indexSet.size() < tournament) {
            Integer index = getInfFromRange(0, population.getSize());
            if(!indexSet.contains(index)) {
                indexSet.add(index);
                evaluate = getScheduleEvaluation(index);
                if(evaluate < evaluationMin) {
                    indexMin = index;
                    evaluationMin = evaluate;
                }
            }
        }
        return indexMin;
    }

    private int getInfFromRange(int start, int end) {
        return (int) (random.nextDouble() * end);
    }

    private double getScheduleEvaluation(Integer index) {
        return population.getScheduleByIndex(index).evaluate();
    }

    private double getProbability() {
        return ThreadLocalRandom.current().nextDouble();
    }

    private Resource getRandomCapableResource(List<Resource> resources, int numberOfResources) {
        return resources.get(getRandomIndex(numberOfResources));
    }

    private int getRandomIndex(int numberOfResources) {
        return (int) (random.nextDouble() * numberOfResources);
    }

    public List<List<Integer>> getMinAvgMax() {
        return minAvgMax;
    }
}
