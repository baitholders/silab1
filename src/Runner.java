import msrcpsp.evaluation.BaseEvaluator;
import msrcpsp.evaluation.DurationEvaluator;
import msrcpsp.io.MSRCPSPIO;
import msrcpsp.scheduling.RandomSchedule;
import msrcpsp.scheduling.Resource;
import msrcpsp.scheduling.Schedule;
import msrcpsp.scheduling.Task;
import msrcpsp.scheduling.greedy.Greedy;
import msrcpsp.validation.BaseValidator;
import msrcpsp.validation.CompleteValidator;

import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Runner class to help with understanding of the library.
 */
public class Runner {

    private static final Logger LOGGER = Logger.getLogger(Runner.class.getName());
    private static final String definitionFile = "assets/def_small/100_5_22_15.def";
    private static final String writeFile = "assets/solutions_small/10_3_5_3.sol";

    public static void main(String[] args) {

        // read definition file into Schedule object
        MSRCPSPIO reader = new MSRCPSPIO();
        Schedule schedule = reader.readDefinition(definitionFile);
        if (null == schedule) {
            LOGGER.log(Level.WARNING, "Could not read the Definition " + definitionFile);
        }

        System.out.println("Tasks");
        for (Task t : schedule.getTasks())
            System.out.println(t.toString());

        System.out.println("Resources");
        for (Resource r : schedule.getResources())
            System.out.println(r.toString());

        // get array of upper bounds of each task assignment, that does
        // violate skill constraint
        int[] upperBounds = schedule.getUpperBounds(schedule.getTasks().length);

        System.out.println("Upped bounds: ");
        for (int i : upperBounds)
            System.out.print(upperBounds[i] + ", ");
        System.out.println();
        // create an evaluator


        Task[] tasks = schedule.getTasks();
        Resource[] resources = schedule.getResources();

        // create arbitrary schedule
        schedule.assign(tasks[0], resources[0]);
        schedule.assign(tasks[1], resources[1]);
        schedule.assign(tasks[2], resources[2]);
        schedule.assign(tasks[3], resources[0]);
        schedule.assign(tasks[4], resources[1]);
        schedule.assign(tasks[5], resources[2]);
        schedule.assign(tasks[6], resources[0]);
        schedule.assign(tasks[7], resources[1]);
        schedule.assign(tasks[8], resources[2]);
        schedule.assign(tasks[9], resources[0]);


        System.out.println("Tasks");
        for (Task t : schedule.getTasks())
            System.out.println(t.toString());

        System.out.println("Resources");
        for (Resource r : schedule.getResources())
            System.out.println(r.toString());

        // create greedy algorithm to set timestamps
        Greedy greedy = new Greedy(schedule.getSuccesors());
        for (boolean b : schedule.getSuccesors())
            System.out.print(b + ", ");
        // set starts of each task using greedy algorithm
        System.out.println("\n======================================================");


        greedy.buildTimestamps(schedule);

        // validate schedule, it results in a failure (keep in mind, that
        // the schedule was created by hand)
        BaseValidator validator = new CompleteValidator();
        System.out.println(validator.validate(schedule));
        System.out.println(validator.getErrorMessages());


        // create schedule randomly, while keeping constraints
//        RandomSchedule generator = new RandomSchedule();
        RandomSchedule generator = new RandomSchedule(new Random(System.currentTimeMillis()));
        generator.randomTasksAssignment(schedule);

        // set timestamps in greedy manner
        System.out.println("===========================================RANDOM=========================================");
        greedy.buildTimestamps(schedule);
        System.out.println("Tasks");
        for (Task t : schedule.getTasks())
            System.out.println(t.toString());

        System.out.println("Resources");
        for (Resource r : schedule.getResources())
            System.out.println(r.toString());
        // validate schedule, this time it results in a success
        System.out.println(validator.validate(schedule));
        System.out.println(validator.getErrorMessages());

        BaseEvaluator evaluator = new DurationEvaluator(schedule);
        System.out.println("Evaluate: " + evaluator.evaluate());

        // save to a file
        try {
            reader.write(schedule, writeFile);
        } catch (IOException e) {
            System.out.print("Writing to a file failed");
        }

        System.out.println(schedule.resourcesAssigments() + ", Evaluate: " + schedule.evaluate());

    }
}
